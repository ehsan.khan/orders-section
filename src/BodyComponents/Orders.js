import React from 'react'
import { Input } from 'antd';
import { Select, Button, Space } from 'antd';
import { DownloadOutlined } from '@ant-design/icons';
import { Row, Col, Divider } from 'antd';

const style = { background: '#0092ff', padding: '8px 0' };

const { Option } = Select;
function handleChange(value) {
  console.log(`selected ${value}`);
}

class ButtonSize extends React.Component {
  state = {
    size: 'default',
  };

  handleSizeChange = e => {
    this.setState({ size: e.target.value });
  };
  render() {
    const { size } = this.state;
    return (
      <Button type="default" icon={<DownloadOutlined />} size={size}>
        Download
      </Button>
    )
  };
}






export const Orders = () => {
  return (
    <div className='main'>

      <Row >
        <Col xs={24} sm={24} md={8} lg={8} xl={8}><h1>Orders</h1></Col>

        <Col xs={24} sm={24} md={4} lg={4} xl={4}>
          <Input placeholder="Search" size='middle' />
        </Col>
        <Col xs={24} sm={24} md={4} lg={4} xl={4}>
          <Button>Search</Button>
        </Col>
        <Col xs={24} sm={24} md={4} lg={4} xl={4}>
          <Select defaultValue="All" style={{ width: 120 }} onChange={handleChange}>
            <Option value="All">All</Option>
            <Option value="This Week">This Week</Option>
            <Option value="This Month">This Month</Option>
            <Option value="Last 3 Months">Last 3 Months</Option>
          </Select>
        </Col>
        <Col xs={24} sm={24} md={4} lg={4} xl={4}>
          <ButtonSize />
        </Col>
      </Row>
    </div>
   )
}
    




    
 




