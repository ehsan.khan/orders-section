import React from 'react'
import { Button, Table, Tag, Space } from 'antd';


class Demo extends React.Component {
  state = {
    bottom: 'bottomcenter',
  };
  render() {
    return (
      <Table
        columns={columns}
        pagination={{ position: [this.state.bottom] }}
        dataSource={data}
      />
    )
  }
}


class ButtonSize extends React.Component {
  state = {
    size: 'small',
  };

  handleSizeChange = e => {
    this.setState({ size: e.target.value });
  };
  render() {
    const { size } = this.state;
    return (
      <Button size={size}>View</Button>
    )
  }
}
const columns = [
  {
    title: 'Order',
    dataIndex: 'order',
    key: 'order',
  },
  {
    title: 'Product',
    dataIndex: 'product',
    key: 'product',
  },
  {
    title: 'Customer',
    dataIndex: 'customer',
    key: 'customer',
  },
  {
    title: 'Date',
    dataIndex: 'date',
    key: 'date',
  },
  {
    title: 'Status',
    key: 'status',
    dataIndex: 'status',
    render: status => (
      <>
        {status.map(tag => {
          let color = 'yellow';
          return (
            <Tag color={color} key={tag}>
              {tag.toUpperCase()}
            </Tag>
          );
        })}
      </>
    ),
  },
  {
    title: 'Total',
    dataIndex: 'total',
    key: 'total',
  },
  {
    title: '',
    key: 'action',
    render: () => (
      <Space size="middle">
        <ButtonSize />
      </Space>
    ),
  },
];


const data = [
  {
    key: '1',
    order: '#15345',
    product: 'Consectetur adipiscing elit',
    customer: 'Dylan Ambrose',
    date: '16 Oct 03:16 AM',
    status: ['pending'],
    total: '	$96.20'
  },
];

export const Pending = () => {
  return (
    <div>
      <Demo />
    </div>
  )
}
