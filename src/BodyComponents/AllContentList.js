import React from 'react'
import { Button, Table, Tag, Space } from 'antd';


class Demo extends React.Component {
  state = {
    bottom: 'bottomcenter',
  };
  render() {
    return (
      <Table
        columns={columns}
        pagination={{ position: [this.state.bottom] }}
        dataSource={data}
      />
    )
  }
}



class ButtonSize extends React.Component {
  state = {
    size: 'small',
  };

  handleSizeChange = e => {
    this.setState({ size: e.target.value });
  };
  render() {
    const { size } = this.state;
    return (
      <Button size={size}>View</Button>
    )
  }
}
const columns = [
  {
    title: 'Order',
    dataIndex: 'order',
    key: 'order',
  },
  {
    title: 'Product',
    dataIndex: 'product',
    key: 'product',
  },
  {
    title: 'Customer',
    dataIndex: 'customer',
    key: 'customer',
  },
  {
    title: 'Date',
    dataIndex: 'date',
    key: 'date',
  },
  {
    title: 'Status',
    key: 'status',
    dataIndex: 'status',
    render: status => (
      <>
        {status.map(tag => {
          let color = tag == 'paid' ? 'green' : tag == 'pending' ? 'yellow' : 'red';
          return (
            <Tag color={color} key={tag}>
              {tag.toUpperCase()}
            </Tag>
          );
        })}
      </>
    ),
  },
  {
    title: 'Total',
    dataIndex: 'total',
    key: 'total',
  },
  {
    title: '',
    key: 'action',
    render: () => (
      <Space size="middle">
        <ButtonSize />
      </Space>
    ),
  },
];

const data = [
  {
    key: '1',
    order: '#15346',
    product: 'Lorem ipsum dolor sit amet eget volutpat erat..',
    customer: 'John Sanders',
    date: '17 Oct 2:16 PM',
    status: ['paid'],
    total: '$259.35'
  },
  {
    key: '2',
    order: '#15345',
    product: 'Consectetur adipiscing elit',
    customer: 'Dylan Ambrose',
    date: '16 Oct 03:16 AM',
    status: ['pending'],
    total: '	$96.20'
  },
  {
    key: '3',
    order: '#15344',
    product: 'Pellentesque diam imperdiet',
    customer: 'Teresa Holland',
    date: '16 Oct 01:16 AM',
    status: ['paid'],
    total: '$123.00'

  },
  {
    key: '4',
    order: '#15343',
    product: 'Vestibulum a accumsan lectus sed mollis ipsum..',
    customer: 'Jayden Massey',
    date: '	15 Oct 8:07 PM',
    status: ['paid'],
    total: '	$199.00'

  },
  {
    key: '5',
    order: '#15342',
    product: 'Justo feugiat neque',
    customer: 'Reina Brooks',
    date: '12 Oct 04:23 PM',
    status: ['cancelled'],
    total: '	$59.00'
  },
  {
    key: '6',
    order: '#15341',
    product: 'Morbi vulputate lacinia neque et sollicitudin..',
    customer: 'Raymond Atkins',
    date: '	11 Oct 11:18 AM',
    status: ['paid'],
    total: '	$678.26'

  },
];


export const AllContentList = () => {
  return (
    <div>
      <Demo />
    </div>
  )
}
