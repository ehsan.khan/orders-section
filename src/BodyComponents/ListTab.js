import React from 'react'
import { Tabs } from 'antd';
import { AllContentList } from "./AllContentList";
import { Paid } from "./Paid";
import { Pending } from "./Pending";
import { Cancelled } from "./Cancelled";


const { TabPane } = Tabs;

function callback(key) {
  console.log(key);
}

const Demo = () => (
  <Tabs defaultActiveKey="1" centered onChange={callback} size='large'>

    <TabPane tab="All" key="1">
      <AllContentList />
    </TabPane>
    <TabPane tab="Paid" key="2">
      <Paid />
    </TabPane>
    <TabPane tab="Pending" key="3">
      <Pending />
    </TabPane>
    <TabPane tab="Cancelled" key="4">
      <Cancelled />
    </TabPane>
  </Tabs>
);


export const ListTab = () => {
  return (
    <div>
      <Demo />
    </div>
  )
}
