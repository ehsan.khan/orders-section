import './App.css';
import { Orders } from "./BodyComponents/Orders";
import { ListTab } from "./BodyComponents/ListTab";
import 'antd/dist/antd.css';

function App() {
  return (
    <>
      <Orders />
      <ListTab />
    </>
  );
}

export default App;
